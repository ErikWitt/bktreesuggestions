package controllers

import play.api.mvc._
import play.api.templates.Html
import java.net.URLEncoder
import scala.concurrent.{ExecutionContext, Future}
import models.searchengine.SearchService
import scala.Some
import ExecutionContext.Implicits.global
import models.suggestengine.SuggestService

object Application extends Controller {

  def index(query: Option[String] = None) = Action {
    query match {
      case None => Ok(views.html.index(None, None))
      case Some(queryString) => {

        val searchList: List[(String, String)] = List(
          ("Google", s"https://ajax.googleapis.com/ajax/services/search/web?v=1.0&q=${URLEncoder.encode(queryString, "UTF-8")}")
        )

        val searchResults: List[Future[Option[String]]] = SearchService.performSearch(searchList)
        val responsesFuture: Future[List[Option[String]]] = Future.sequence(searchResults)
        val suggestion = SuggestService.suggestion(queryString)

        Async {
          responsesFuture.map {
            case Nil => BadGateway
            case l: List[Option[String]] => {
              val l2 = l.map(_.getOrElse(""))
              Ok(views.html.index(Some(Html(l2.mkString(""))), suggestion))
            }
          }
        }
      }
    }
  }
}