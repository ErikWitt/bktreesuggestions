package models.suggestengine

object SuggestService {

  val bkTree = {
    var tree: BKTree[String] = EmptyBKTree
    Dictionary.words foreach {
      w => tree = tree add w
    }
    tree
  }

  def suggestion(input: String): Option[String] = {
    bkTree.query(input, 2).headOption
  }
}

object Dictionary {
  val words = scala.io.Source.fromFile("/Users/erikwitt/bktreesuggestions/BKTreeSuggestion/app/models/suggestengine/dict.txt").getLines().toList
}
