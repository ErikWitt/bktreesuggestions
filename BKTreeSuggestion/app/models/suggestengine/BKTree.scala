package models.suggestengine
import MetricSpace._

trait BKTree[+T] {
  def isEmpty: Boolean
  def element: T
  def children: Map[Int, BKTree[T]]
  def add[U >: T : WithMetricSpace](elem: U): BKTree[U]
  def query[U >: T : WithMetricSpace](elem: U, radius: Int): List[U]
}

case class NonemptyBKTree[+T](element: T, children: Map[Int, BKTree[T]]) extends BKTree[T] {
  val isEmpty = false

  def add[U >: T : WithMetricSpace](elem: U): BKTree[U] = {
    val distance = elem.distance(element)
    distance match {
      case 0 => this
      case d if children.isDefinedAt(d) => NonemptyBKTree(element, children updated(d, children(d).add(elem)))
      case d => NonemptyBKTree(element, children + (d -> NonemptyBKTree(elem, Map.empty)))
    }
  }
  def query[U >: T : WithMetricSpace](elem: U, radius: Int): List[U] = {
    val distance = elem.distance(element)
    val recResult = for {
      dist <- ((distance - radius) to (distance + radius)).toList
      foundElems <- children.getOrElse(dist, EmptyBKTree).query(elem, radius)
    } yield foundElems
    if (distance <= radius) element :: recResult else recResult
  }
}

case object EmptyBKTree extends BKTree[Nothing] {
  val isEmpty = true
  def element = throw new Error("EmptyBKTree.element")
  def children = Map.empty

  def add[U >: Nothing : WithMetricSpace](elem: U): BKTree[U] = NonemptyBKTree(elem, Map.empty)
  def query[U >: Nothing : WithMetricSpace](elem: U, radius: Int): List[U] = Nil
}

trait MetricSpace[T] {
  def distance(other: T): Int
}

object MetricSpace {
  type WithMetricSpace[T] = T => MetricSpace[T]

  implicit def string2MetricSpace(s: String) = new MetricSpace[String] {
    def distance(other: String): Int = LevenshteinDistance.distance(s, other)
  }
}


