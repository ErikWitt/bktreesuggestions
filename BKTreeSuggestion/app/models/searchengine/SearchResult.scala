package models.searchengine

trait SearchResult {
  def title: String
  def url: String

  def asHtmlLink = s"""<a href="$url">$title</a>"""
}
