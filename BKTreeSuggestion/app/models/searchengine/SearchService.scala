package models.searchengine

import scala.concurrent.{ExecutionContext, Future}
import scala.Option
import play.api.libs.json._
import play.api.libs.ws
import play.api.libs.ws.WS
import play.api.libs.json.JsSuccess
import scala.Some
import models.searchengine.GoogleResult._
import ExecutionContext.Implicits.global

object SearchService {


  def performSearch(searchList: List[(String, String)]): List[Future[Option[String]]] = {
    for {
      (searchProvider, url) <- searchList
      responseFuture: Future[Option[JsValue]] = search(url)
      resultsFuture: Future[Option[String]] = responseFuture.map(jsValueOpt =>
        jsValueOpt.map(jsValue => {
          val parsedJson: JsResult[List[SearchResult]] = Json.fromJson[List[SearchResult]](jsValue)
          val response = parsedJson match {
            case JsSuccess(results, _) => formatSearchResults(results)
            case JsError(_) => s"error while parsing response from $searchProvider"
          }
          s"<h2>$searchProvider results:</h2>$response"
        }))
    } yield resultsFuture
  }

  def search(url: String): Future[Option[JsValue]] = {
    val responseFuture: Future[ws.Response] = WS.url(url).get()

    responseFuture.map {
      response =>
        if (response.status == 200) Some(response.json) else None
    }
  }

  def formatSearchResults(results: List[SearchResult]) = {
    results.map(_.asHtmlLink).mkString("<p>", "<br />", "</p>")
  }
}
