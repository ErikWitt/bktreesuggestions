package models.searchengine

import play.api.libs.json._

case class GoogleResult(title: String, url: String) extends SearchResult

object GoogleResult {

  implicit val googleResultReader = Reads[GoogleResult] {
    json =>
      for {
        title <- (json \ "title").validate[String]
        url <- (json \ "url").validate[String]
      } yield GoogleResult(title, url)
  }

  implicit val googleResultsReader = Reads[List[SearchResult]] {
    json =>
      for {
        list <- (json \ "responseData" \ "results").validate[List[GoogleResult]]
      } yield list
  }

}